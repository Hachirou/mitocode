package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IUsuarioDAO;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Service
public class UsuarioServiceImpl implements IUsuarioService{

	@Autowired
	private IUsuarioDAO dao;
	
	@Override
	public Usuario registrar(Usuario t) {	
		return dao.save(t);
	}

	@Override
	public Usuario modificar(Usuario t ) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.delete(id);
	}

	@Override
	public Usuario listarId(int id) {
		return dao.findOne(id);
	}

	@Override
	public List<Usuario> listar() {
		return dao.findAll();
	}

	@Override
	public Page<Usuario> listarPageable(Pageable pageable) {
		return dao.findAll(pageable);
	}
}
