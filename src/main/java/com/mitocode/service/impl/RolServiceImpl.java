package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IRolDAO;
import com.mitocode.model.Rol;
import com.mitocode.service.IRolService;

@Service
public class RolServiceImpl implements IRolService{
	
	@Autowired
	private IRolDAO dao;
	
	@Override
	public Rol registrar(Rol t) {	
		return dao.save(t);
	}

	@Override
	public Rol modificar(Rol t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.delete(id);
	}

	@Override
	public Rol listarId(int id) {
		return dao.findOne(id);
	}

	@Override
	public List<Rol> listar() {
		return dao.findAll();
	}

	@Override
	public Page<Rol> listarPageable(Pageable pageable) {
		return dao.findAll(pageable);
	}

}
